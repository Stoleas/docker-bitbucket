FROM openjdk:8-alpine
MAINTAINER stoleas

# Bitbucket Environments
ENV BB_INSTALL_DIR        /opt/atlassian/bitbucket/
ENV BB_HOME               /opt/atlassian/bitbucket-home/
ENV BB_ARTIFACT_URL       https://www.atlassian.com/software/stash/downloads/binary/atlassian-bitbucket-4.14.5.tar.gz
ENV BB_ARTIFACT_NAME      atlassian-bitbucket-4.14.5

# Expose APP ports
EXPOSE 7990
EXPOSE 7999

# Install packages and dependencies
RUN apk upgrade  --update && \
    apk add      --update curl wget ca-certificates bash git perl openssl

# Setup Bitbucket
RUN mkdir -p ${BB_INSTALL_DIR} ${BB_HOME}/shared/data  && \
    wget  -O /opt/${BB_ARTIFACT_NAME}.tgz ${BB_ARTIFACT_URL} && \
    tar   -xzf /opt/${BB_ARTIFACT_NAME}.tgz -C ${BB_INSTALL_DIR} && \
    rm    /opt/${BB_ARTIFACT_NAME}.tgz ${BB_INSTALL_DIR}/${BB_ARTIFACT_NAME}/bin/set-bitbucket-user.sh ${BB_INSTALL_DIR}/${BB_ARTIFACT_NAME}/bin/setenv.sh ${BB_INSTALL_DIR}/${BB_ARTIFACT_NAME}/conf/server.xml

ADD bin/*           ${BB_INSTALL_DIR}/${BB_ARTIFACT_NAME}/bin/
ADD conf/server.xml ${BB_INSTALL_DIR}/${BB_ARTIFACT_NAME}/conf/server.xml

CMD ["/opt/atlassian/bitbucket/atlassian-bitbucket-4.14.5/bin/start-bitbucket.sh", "-fg"]

# Build this image
# docker build -t bitbucket:4.14.5 .

# Test - Example Run
# docker run --expose 7990 --expose 7999 -p 7990:7990 -p 7999:7999 bitbucket:4.14.5

# Source
# https://bitbucket.org/Stoleas/docker-bitbucket/src